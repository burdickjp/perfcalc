# turboSizingCalculator

tools for sizing turbocharger for internal combustion engines.

## Compressor maps

compressor maps are stored in the `mapscomp` folder, along with a `JSON` configuration file which contains scaling information for overlaying on the Bokeh plot.

## Engine files

Data from ECU logging and dyno graphs can be interpolated into a `JSON` file for overlaying into a Bokeh plot.

## Generating engine curves from scratch