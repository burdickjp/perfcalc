podman run \
	-p 8888:8888 \
	-v .:/home/jovyan/work \
	-e JUPYTER_ENABLE_LAB=yes \
	jupyter/datascience-notebook
